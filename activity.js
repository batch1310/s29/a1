const express = require("express");
const app  = express();
const PORT = 3000;

app.use(express.json());
app.use(express.urlencoded({extended: true}));

// homepage route
app.get("/home", (req, res) => res.send(`Welcome to home page`));

// users access route
app.post("/users", (req, res) => res.send(req.body));

// delete user
app.delete("/delete-user", (req, res) => res.send(`User ${req.body.username} has been deleted.`));

app.listen(PORT, () => console.log(`Server is running at port ${PORT}`));